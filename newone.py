# /usr/bin/python3

# --------------------- python imports --------------------- #
import ast

# --------------------- global variables --------------------- #
# list of terms in function on left hand side
global_lhs_terms = []
# list of terms in function on right hand side
global_rhs_terms = []
# list to hold the sequence of substitutions
global_replacements = []


# --------------------- helper functions --------------------- #
# check if term is a function
def is_function(term: object):
    if 'args' in term._fields:
        if term.func.id.islower():
            return True
        else:
            exit_program(msg="No")
    return False


# check if term is a variable
def is_variable(term: object):
    if 'id' in term._fields:
        if term.id.isupper():
            return True
    return False


# check if term is a constant
def is_constant(term: object):
    if 'id' in term._fields:
        if term.id.islower():
            return True
    return False


# function to terminate program with message
def exit_program(msg: str):
    print(msg)
    raise SystemExit


# check if equation on left is equal to equation on right
def not_equal(lhs_terms, rhs_terms):
    counter = 0
    # iterate over all the terms and check for the equality on both sides
    for index in range(len(lhs_terms)):
        # check for equality of variables
        if is_variable(lhs_terms[index]) and is_variable(rhs_terms[index]):
            if lhs_terms[index].id == rhs_terms[index].id:
                counter = counter + 1

        # check for equality of constants
        if is_constant(lhs_terms[index]) and is_constant(rhs_terms[index]):
            if lhs_terms[index].id == rhs_terms[index].id:
                counter = counter + 1

        # check for equality of functions
        if is_function(lhs_terms[index]) and is_function(rhs_terms[index]):
            if get_function_expr(lhs_terms[index]) == get_function_expr(rhs_terms[index]):
                counter = counter + 1

        # terminate program if there is a function on one side
        # and constant on other side
        if (is_function(lhs_terms[index]) and is_constant(rhs_terms[index])) or \
                (is_function(rhs_terms[index]) and is_constant(lhs_terms[index])):
            print("Cannot replace a constant with a function.")
            exit_program(msg="No")

    if counter == len(rhs_terms):
        return False
    return True


# get the values in nested arrays
# i.e get the atomic value in case of un-necessary
# nesting of values takes place
def get_the_subs_array(replacement):
    if isinstance(replacement, list) and len(replacement) == 1:
        return get_the_subs_array(replacement[0])
    else:
        return replacement


# fix the substitutions according to the current substitution
def normalize_global_replacements(target_replacement):
    for replacement in global_replacements:
        if is_variable(replacement[1]):
            if replacement[1].id == target_replacement[0].id:
                replacement[1] = target_replacement[1]


# --------------------- representation functions --------------------- #
# return representation of a term which is a function
def get_function_expr(term: ast.Name) -> list:
    repr_list = []
    if is_function(term):
        repr_list.append(term.func.id)
        repr_list.append('(')
        index = 0
        for argument in term.args:
            repr_list += get_function_expr(argument)
            if index != len(term.args) - 1:
                repr_list.append(',')
            index = index + 1
        repr_list.append(')')

    if is_variable(term) or is_constant(term):
        repr_list.append('{}'.format(term.id))
        repr_list.append(',')

    if repr_list:
        if repr_list[len(repr_list) - 1:][0] == ',':
            repr_list.pop(len(repr_list) - 1)

    return repr_list


# return variable/constant representation
def get_variable_expr(term):
    return term.id


# get the iterable substitution representation
def get_substitution_iterable(replacement: list) -> list:
    subs_repr = []
    for sub in replacement:
        if isinstance(sub, list):
            subs_repr.append(get_substitution_iterable(sub))
        else:
            if is_variable(sub) or is_constant(sub):
                subs_repr.append(get_variable_expr(sub))
            else:
                subs_repr.append(get_function_expr(sub))
        if ('->' not in subs_repr) and (not isinstance(sub, list)):
            subs_repr.append('->')

    return subs_repr


# print the substitution
def print_substitution(subs_repr: list) -> str:
    printable = ''
    for i in subs_repr:
        if isinstance(i, list):
            printable = "{}{}".format(printable, print_substitution(i))
        else:
            if i == '-->':
                printable = "{}{}{}{}".format(printable, " ", i, " ")
            else:
                printable = "{}{}".format(printable, i)

    return printable


# --------------------- substitution functions --------------------- #

# substitute a variable in function
def substitute_in_function(term: object, term1: object, term2: object) -> bool:
    # iterate over the terms in functions
    for index in range(len(term.args)):
        # if the term itself is a function
        # then get into it via recursion
        if is_function(term.args[index]):
            substitute_in_function(term.args[index], term1, term2)
        # if current term is a variable
        # which is to be replaced
        # then, perform substitution
        elif is_variable(term.args[index]):
            if term1.id == term.args[index].id:
                term.args[index] = term2
            else:
                continue
    return term


# function to perform substitution on the
# terms of left-hand-side and on right-hand-side
def substitute(term1: object, term2: object, node_list: list) -> list:
    updated_nodes = []
    # iterate over all the nodes
    for term in node_list:
        # if term2 is a variable
        if is_variable(term2):
            # if current node is a variable
            if is_variable(term):
                # if term1 = term
                if term.id == term1.id:
                    # substitute term2 in current node
                    term = term2
            # if current node is a function then,
            # if term1 occurs in term(current node)
            # substitute it with term2
            elif is_function(term):
                term = substitute_in_function(term, term1, term2)
        # if term2 is constant
        elif is_constant(term2):
            # if current term is variable
            # then, substitute
            if is_variable(term):
                if term.id == term1.id:
                    term = term2
            # if current term is constant
            # with same name
            elif is_constant(term):
                if term.id == term1.id:
                    term = term2
            # if current term is a function
            else:
                term = substitute_in_function(term, term1, term2)
        # if term2 is a function
        else:
            # if term(current node) is a variable
            if is_variable(term):
                if term1.id == term.id:
                    term = term2
            # if term is a function
            elif is_function(term):
                term = substitute_in_function(term, term1, term2)

        # update the terms on intended side
        updated_nodes.append(term)
    # return the updated list of nodes
    return updated_nodes


# --------------------- unification related functions --------------------- #
# if two terms are functions then,
# check if they are unifiable
def check_if_functions_unifyable(term1: ast.Call, term2: ast.Call):
    # check if initials are same
    if term1.func.id == term2.func.id:
        # check if number of arguments are same
        if len(term1.args) == len(term2.args):
            return True
    return False


# check if functions are initially unifiable
def check_if_initially_unifyable(lhs: str, rhs: str) -> tuple:
    # parse the equation on left-hand-side
    lhs_ast = ast.parse(lhs)
    # parse the equation on right-hand-side
    rhs_ast = ast.parse(rhs)
    # if both the inputted values are functions
    if is_function(lhs_ast.body[0].value) and is_function(rhs_ast.body[0].value):
        # check if they are unifiable
        if check_if_functions_unifyable(lhs_ast.body[0].value, rhs_ast.body[0].value):
            return True, lhs_ast, rhs_ast
        else:
            return False, None, None
    else:
        return True, lhs_ast, rhs_ast


# check if unification is possible between
# two variables
def unify_variables(term1: object, term2: object) -> list:
    # check if t2 a variable or constant
    if is_variable(term2):
        # if t1 = t2 return NIL
        if term1.id == term2.id:
            return True, None
        else:
            # replace t1 with t2 => [t2/t1]
            return True, [term1, term2]
    elif is_constant(term2):
        # replace t1 with t2 => [t2/t1]
        return True, [term1, term2]
    else:
        return None, None


# check if unification is possible between
# a variable and a function
def unify_function(term1: object, term2: object) -> bool:
    # if term2 is a function then
    if is_function(term2):
        # check for the arguments of the function
        for argument in term2.args:
            if is_function(argument):
                return unify_function(term1, argument)
            elif is_variable(argument):
                # if a variable occurs in a function
                if term1.id == argument.id:
                    print("Cannot unify because a variable occurs in function argument.")
                    exit_program(msg="No")
                else:
                    continue
        return True
    else:
        return None


def check_terms_for_substitution(term1: object, term2: object):
    # if term2 is a variable
    # and proceed towards possible unification
    any_substitution, substution_set = unify_variables(term1, term2)
    if any_substitution:
        if substution_set:
            return any_substitution, substution_set
        return any_substitution, None

    # if term2 is a function
    # and proceed towards possible unification
    any_substitution = unify_function(term1, term2)
    if any_substitution:
        # replace t1 with t2 => [t2/t1]
        return any_substitution, [term1, term2]
    else:
        return None, None


def unify(lhs_terms: list, rhs_terms: list) -> tuple:
    for index in range(len(lhs_terms)):
        any_substitutions, substitutions = unify_with_occurs_check(lhs_terms[index], rhs_terms[index])
        if any_substitutions is None:
            continue
        elif any_substitutions and substitutions:
            return True, get_the_subs_array(substitutions)

    if lhs_terms == rhs_terms:
        return True, None
    print("Unification not possible.")
    exit_program("No")


# unify with occurs check
def unify_with_occurs_check(term1: object, term2: object) -> tuple:
    # check if term1 is a variable or constant
    if is_variable(term1):
        return check_terms_for_substitution(term1, term2)
    # if term1 is constant
    elif is_constant(term1):
        # if term2 is constant
        if is_constant(term2):
            # if constants are same then
            # return null
            if term1.id == term2.id:
                return True, None
            else:
                print("Cannot unify constants with different names.")
                exit_program(msg="No")
        # if term2 is a variable then
        # substitute term2 with term1 => [t1/t2]
        if is_variable(term2):
            return True, [term2, term1]
    # if term1 is a function
    elif is_function(term1):
        # if term2 is a function, then exit
        if is_function(term2):
            if term1 == term2:
                return None, None
            else:
                # if term1 and term2 are not identical return No
                # as unification is not possible
                is_unifyable = check_if_functions_unifyable(term1, term2)
                if not is_unifyable:
                    print("Unification not possible.")
                    exit_program(msg="No")
                else:
                    return unify(lhs_terms=term1.args, rhs_terms=term2.args)

        # if term2 is a variable
        else:
            return check_terms_for_substitution(term2, term1)
    else:
        print("Unification not possible.")
        exit_program(msg="No")


# unify terms in the inputted expressions
def do_unification(lhs_terms, rhs_terms):
    # check if expressions on both side are equal
    # loop until expressions becomes equal
    while not_equal(lhs_terms, rhs_terms):
        # proceed for unification
        statue, replacements = unify(lhs_terms=lhs_terms, rhs_terms=rhs_terms)
        if statue and replacements:
            if replacements not in global_replacements:
                normalize_global_replacements(replacements)
                global_replacements.append(replacements)

            # make substitutions on left-hand-side
            returned_lhs_nodes = substitute(term1=replacements[0], term2=replacements[1], node_list=lhs_terms)
            if returned_lhs_nodes:
                # update nodes on left-hand-side
                lhs_terms.clear()
                lhs_terms += returned_lhs_nodes

            # make substitutions on right-hand-side
            returned_rhs_nodes = substitute(term1=replacements[0], term2=replacements[1], node_list=rhs_terms)
            if returned_rhs_nodes:
                # update nodes on right-hand-side
                rhs_terms.clear()
                rhs_terms += returned_rhs_nodes

    for repl in global_replacements:
        print(print_substitution(get_substitution_iterable(repl)))
    print("Yes")


# --------------------- Main Function --------------------- #
if __name__ == '__main__':
    # input the expressions
    print("input expressions on left-hand-side")
    lhs = str(input().strip())
    print("input expressions on right-hand-side")
    rhs = str(input().strip())
    try:
        # check if unification is possible
        is_unification_possible, left_ast, right_ast = check_if_initially_unifyable(lhs, rhs)
        if not is_unification_possible:
            print("Unification Not Possible.")
            print("No")
        else:
            if is_function(left_ast.body[0].value) and is_function(right_ast.body[0].value):
                global_lhs_terms = left_ast.body[0].value.args
                global_rhs_terms = right_ast.body[0].value.args
            else:
                global_lhs_terms.append(left_ast.body[0].value)
                global_rhs_terms.append(right_ast.body[0].value)
            do_unification(lhs_terms=global_lhs_terms, rhs_terms=global_rhs_terms)
    except SyntaxError as se:
        print("invalid expression")
        print(se.text)
    except SystemExit as exi:
        pass
    except BaseException as ex:
        print("Something went Wrong")
