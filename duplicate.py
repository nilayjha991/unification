# /usr/bin/python3

import ast
import copy

global_lhs_terms = []
global_rhs_terms = []
global_replacements = []
global_terms_map = []


def exit_program(msg: str):
    print(msg)
    raise SystemExit


def check_if_functions_unifyable(term1: ast.Call, term2: ast.Call):
    if term1.func.id == term2.func.id:
        if len(term1.args) == len(term2.args):
            return True
    return False


# check if functions are unifiable
def check_if_initially_unifyable(lhs: str, rhs: str) -> tuple:
    initial_letter_lhs = lhs[:1]
    initial_letter_rhs = rhs[:1]
    if (initial_letter_lhs == initial_letter_rhs) and \
            (initial_letter_lhs.islower() and initial_letter_rhs.islower()):
        # make ASTs for both the equations
        left_ast = ast.parse(lhs)
        right_ast = ast.parse(rhs)
        if len(left_ast.body[0].value.args) == len(right_ast.body[0].value.args):
            return True, left_ast, right_ast
        else:
            return False, None, None
    else:
        return False, None, None


def unify_variables(term1: object, term2: object) -> list:
    # check if t2 a variable or constant
    if 'id' in term2._fields:
        # if t1 = t2 return NIL
        if term1.id == term2.id:
            return True, None
        else:
            # replace t1 with t2 => [t2/t1]
            return True, [term1, term2]
    else:
        return None, None


def unify_function(term1: object, term2: object) -> bool:
    # if term2 is a function then
    if 'args' in term2._fields:
        for argument in term2.args:
            if 'args' in argument._fields:
                return unify_function(term1, argument)
            elif 'id' in argument._fields:
                if term1.id == argument.id:
                    exit_program(msg="No")
                else:
                    continue
        return True
    else:
        return None


def check_terms_for_substitution(term1: object, term2: object):
    # check if t2 a variable or constant
    any_substitution, substution_set = unify_variables(term1, term2)
    if any_substitution:
        if substution_set:
            return any_substitution, substution_set
        return any_substitution, None

    # check if term2 is a function
    any_substitution = unify_function(term1, term2)
    if any_substitution:
        # replace t1 with t2 => [t2/t1]
        return any_substitution, [term1, term2]
    else:
        return None, None


def unify_with_occours_check(term1: ast, term2: object) -> list:
    # check if t1 is a variable or constant
    if 'id' in term1._fields:
        return check_terms_for_substitution(term1, term2)
    # if t1 is a function
    elif 'args' in term1._fields:
        # if t2 is a function, the exit
        if 'args' in term2._fields:
            if term1 == term2:
                return None, None
            else:
                # if t1 and t2 are not identical return No
                # as unification is not possible
                is_unifyable = check_if_functions_unifyable(term1, term2)
                if not is_unifyable:
                    exit_program(msg="No")
                else:
                    unify(lhs_terms=term1.args,
                          rhs_terms=term2.args,
                          replacements=global_replacements)
                    return True, None
        # if t2 is a variable
        else:
            return check_terms_for_substitution(term2, term1)
    else:
        exit_program(msg="No")


def find_unification(lhs_term, rhs_term):
    any_substitutions, substitutions = unify_with_occours_check(lhs_term, rhs_term)
    if any_substitutions:
        return True, substitutions
    return False, None


def unify(lhs_terms,
          rhs_terms,
          replacements=global_replacements) -> tuple:

    for index in range(len(lhs_terms)):
        any_substitutions, substitutions = unify_with_occours_check(lhs_terms[index], rhs_terms[index])
        if any_substitutions and substitutions:
            local_substitutions = []
            local_substitutions.append(substitutions)
            for rep in local_substitutions:
                substitute(term1=rep[0], term2=rep[1], node_list=global_lhs_terms)
                # global_lhs_terms =
                substitute(term1=rep[0], term2=rep[1], node_list=global_rhs_terms)
                # global_rhs_terms = rhs_terms

                if rep not in global_replacements:
                    global_replacements.append(rep)
                if global_lhs_terms == global_rhs_terms:
                    return True, replacements
                else:
                    continue
        else:
            if global_lhs_terms == global_rhs_terms:
                return True, replacements
            unify(lhs_terms=global_lhs_terms, rhs_terms=global_rhs_terms)
    return True, replacements


# def substitute(term2, term1):
def function_repr(term: ast.Name) -> list:

    repr_list = []
    if 'func' in term._fields:
        repr_list.append(term.func.id)
        repr_list.append('(')
        index = 0
        for argument in term.args:

            repr_list += function_repr(argument)

            if index != len(term.args) - 1:
                repr_list.append(',')
            index = index + 1

        repr_list.append(')')

    if 'id' in term._fields:
        repr_list.append('{}'.format(term.id))

        repr_list.append(',')

    if repr_list[len(repr_list) - 1:][0] == ',':
        repr_list.pop(len(repr_list) - 1)

    return repr_list


def get_function_expr(term: ast.Name):
    return function_repr(term)


def get_variable_expr(term):
    return term.id


def print_substitution(replacement):
    subs_repr = []
    for sub in replacement:
        if isinstance(sub, list):
            subs_repr.append(print_substitution(sub))
        else:
            if 'id' in sub._fields:
                subs_repr.append(get_variable_expr(sub))
            else:
                subs_repr.append(get_function_expr(sub))
        if ('->' not in subs_repr) and (not isinstance(sub, list)):
            subs_repr.append('->')

    return subs_repr


# substitute a variable in function
def substitute_in_function(term: object, term1: object, term2: object) -> bool:
    print(function_repr(term))
    for argument in term.args:
        if 'args' in argument._fields:
            substitute_in_function(argument, term1, term2)
        elif 'id' in argument._fields:
            if term1.id == argument.id:
                argument = copy.deepcopy(term2)
            else:
                continue


def substitute(term1, term2, node_list):
    updated_nodes = []
    # iterate over all the nodes
    for term in global_lhs_terms:
        # if term2 is a variable
        if 'id' in term2._fields:
            # if current node is a variable
            if 'id' in term._fields:
                # if term1 = term
                if term.id == term1.id:
                    # substitute term2 in current node
                    term = copy.deepcopy(term2)
            # if current node is a function then,
            # if term1 occurs in term(current node)
            # substitute it with term2
            else:
                substitute_in_function(term, term1, term2)
                print(function_repr(term))
        #     if term2 is a function
        else:
            # if term(current node) is a variable
            if 'id' in term._fields:
                if term1.id == term.id:
                    term = copy.deepcopy(term2)
            # if term is a function
            else:
                substitute_in_function(term, term1, term2)
                print(function_repr(term))
        updated_nodes.append(term)

    return updated_nodes


if __name__ == '__main__':
    print("input LHS")
    lhs = str(input().strip())
    print("input RHS")
    rhs = str(input().strip())
    is_unification_possible, left_ast, right_ast = check_if_initially_unifyable(lhs, rhs)
    if not is_unification_possible:
        exit_program('No.')
    else:
        global_lhs_terms = left_ast.body[0].value.args
        global_rhs_terms = right_ast.body[0].value.args
        # while global_rhs_terms != global_lhs_terms:
        status, replacements = unify(lhs_terms=global_lhs_terms, rhs_terms=global_rhs_terms)
        print(global_replacements)
        for r in global_replacements:
            print(print_substitution(r))

# --------------------- tested expressions --------------------- #
# single character
# ---------------------------------------------------------------
# f(f(X,Y),X) -- f(f(V,U),g(U,Y))

# f(g(h(a(W,W),b(X,Y),t(m)))) -- f(g(h(X,b(Z,W),Y)))

# f(g(h(X,k(V)))) -- f(g(h(Y,k(F))))

# f(X,f(Y),X) -- f(Y,f(a),f(a))
#| X -> Y: [f(Y,f(Y),Y)]
#| Y -> a: [f(a,f(a),a)]
# ---------------------------------------------------------------
# multi-character
# ---------------------------------------------------------------
# father(X) -- father(Y)
# father(ZX, child(PK)) -- father(Q,P)
#  mgu( ,  )
# h(C,D,g(X,Y)) -- h(Z,D,g(g(A,Y),Z))

# Testing notes:
# 1) For multi-char values don't use keywords reserved by python language like "is", "and", "not", "or" and etc.
# f(X,a) -- f(Y,a)
# f(X,Y,Z) -- f(a,g(t,u),q)
# f(g(X),X) -- f(Y,a)